import {Component, Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {PeopleListService} from "../service/people-list.service";

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.css']
})
export class PeopleListComponent implements OnInit {

  students: string[];

  constructor(private service: PeopleListService) { }

  ngOnInit(): void {
    this.service.getAllStudents()
      .subscribe(l => this.students = l);
  }

}
