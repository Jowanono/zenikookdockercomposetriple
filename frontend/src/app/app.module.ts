import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {PeopleListComponent} from './people-list/people-list.component';
import {HttpClientModule} from "@angular/common/http";
import {PeopleListService} from "./service/people-list.service";

@NgModule({
  declarations: [
    AppComponent,
    PeopleListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    PeopleListService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
